import { RouterModule, Routes } from "@angular/router";
// Navabr links
import { HomeComponent } from "./components/home/home.component";
import { AboutUsComponent } from "./components/about-us/about-us.component";
import { ClientsComponent } from "./components/clients/clients.component";

// Sidebar links
import { PartnersComponent } from "./components/partners/partners.component";
import { TestimonialsComponent } from "./components/testimonials/testimonials.component";
import { ProjectsComponent } from "./components/projects/projects.component";
import { ProductsComponent } from "./components/products/products.component";
import { ContactComponent } from "./components/contact/contact.component";

const APP_ROUTES: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'about-us', component: AboutUsComponent },
  { path: 'clients', component: ClientsComponent },
  { path: 'partners', component: PartnersComponent },
  { path: 'testimonials', component: TestimonialsComponent },
  { path: 'projects', component: ProjectsComponent },
  { path: 'products', component: ProductsComponent },
  { path: 'contact', component: ContactComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'home' }
];


export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);